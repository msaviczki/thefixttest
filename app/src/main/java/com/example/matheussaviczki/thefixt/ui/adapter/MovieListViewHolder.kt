package com.example.matheussaviczki.thefixt.ui.adapter

import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.matheussaviczki.thefixt.network.domain.MovieModel
import com.example.matheussaviczki.thefixt.support.adapter.BaseListAdapter
import com.example.matheussaviczki.thefixt.support.utils.onClick
import com.example.matheussaviczki.thefixt.support.utils.scaleAnimation
import com.example.matheussaviczki.thefixt.support.utils.show
import com.example.matheussaviczki.thefixt.support.utils.visible
import kotlinx.android.synthetic.main.card_detail.view.*

class MovieListViewHolder(itemView : View, private val saveMovieListener : MovieListAdapter.SaveMovieListener)
    : BaseListAdapter.BaseViewHolder<MovieModel>(itemView) {

    override fun bind(item: MovieModel) {
        val request  = RequestOptions()
        request.centerCrop()
        Glide.with(itemView.context)
                .asBitmap()
                .load(item.poster)
                .apply(request)
                .into(itemView.imgPoster)
        itemView.txtTitle.text = item.title
        itemView.txtYear.text = item.year
        itemView.onClick {
            saveMovieListener.showMovieDetailOnClick(item)
        }
    }
}