package com.example.matheussaviczki.thefixt.ui.usecase

import com.example.matheussaviczki.thefixt.network.core.ClientApi
import com.example.matheussaviczki.thefixt.network.core.MovieApi
import com.example.matheussaviczki.thefixt.network.domain.MovieDetailResponse
import com.example.matheussaviczki.thefixt.ui.base.BaseUseCase
import com.example.matheussaviczki.thefixt.ui.base.UseCaseResult
import com.example.matheussaviczki.thefixt.ui.contract.MovieDetailContract

class MovieDetailUseCase : BaseUseCase(), MovieDetailContract.MovieDetailUseCase {

    private val movieApi : MovieApi = ClientApi.movieApi

    override fun searchMovie(movieId: String, resultHandler: UseCaseResult<MovieDetailResponse>) {
        movieApi.getMovieDetail(movieId).handleResult(resultHandler)
    }

}