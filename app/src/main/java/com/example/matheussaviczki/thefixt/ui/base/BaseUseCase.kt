package com.example.matheussaviczki.thefixt.ui.base

import com.example.matheussaviczki.thefixt.network.domain.ErrorResponse
import com.example.matheussaviczki.thefixt.support.utils.SchedulersManager
import com.example.matheussaviczki.thefixt.support.utils.rxObserve
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseUseCase(
        private val pool: CompositeDisposable = CompositeDisposable(),
        private val schedulersManager: SchedulersManager = SchedulersManager()
) : UseCase {

    fun Disposable.addToPool() = pool.add(this)

    override fun cleanUp() = pool.clear()

    fun <T : BaseResponse> Single<T>.handleResult(resultHandler: UseCaseResult<T>) {
        rxObserve(
            { resultHandler.onSuccess(it) },
            { resultHandler.onError(ErrorResponse()) },
                    schedulersManager.getProcessScheduler(),
                    schedulersManager.getAndroidScheduler()
            ).addToPool()
        }

}