package com.example.matheussaviczki.thefixt.support.constants

object ApiConstants {
    const val apiKey = "GuUfWRH4tArq0TYvND18DMVZsQGCtWI34i7Y4dqM"
    const val clientKey = "7LA5xwLx6C1iLfV7yx2CunvVe0OYCIT8WIIKFBJ4"
    const val parseUrl = "https://parseapi.back4app.com/"

    const val table = "Movie"
    const val title = "title"
    const val year = "year"
    const val poster = "poster"
    const val movieId = "imdbID"
    const val saved = "isSaved"
}