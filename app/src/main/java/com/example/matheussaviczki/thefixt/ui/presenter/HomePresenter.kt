package com.example.matheussaviczki.thefixt.ui.presenter

import com.example.matheussaviczki.thefixt.network.domain.ErrorResponse
import com.example.matheussaviczki.thefixt.network.domain.MovieResponse
import com.example.matheussaviczki.thefixt.ui.base.UseCaseResult
import com.example.matheussaviczki.thefixt.ui.contract.HomeContract
import com.example.matheussaviczki.thefixt.ui.usecase.HomeUseCase


class HomePresenter(private val homeView : HomeContract.HomeView) : HomeContract.HomePresenter {

    private val homeUseCase : HomeContract.HomeUseCase = HomeUseCase()

    override fun searchMovies(title: String) {
        homeUseCase.searchMovies(title, object : UseCaseResult<MovieResponse>() {
            override fun onSuccess(apiResponse: MovieResponse) {
                homeView.showMovies(apiResponse.movieModelList)
            }

            override fun onError(errorModel: ErrorResponse) {
                homeView.onError()
            }
        })
    }
}