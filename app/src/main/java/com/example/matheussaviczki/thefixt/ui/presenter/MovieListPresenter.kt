package com.example.matheussaviczki.thefixt.ui.presenter

import com.example.matheussaviczki.thefixt.network.domain.MovieModel
import com.example.matheussaviczki.thefixt.support.constants.ApiConstants
import com.example.matheussaviczki.thefixt.support.repositories.entity.MovieDataSource
import com.example.matheussaviczki.thefixt.support.repositories.entity.MovieRepository
import com.example.matheussaviczki.thefixt.support.repositories.room.MovieDataBase
import com.example.matheussaviczki.thefixt.ui.contract.MovieListContract
import com.parse.ParseObject
import com.parse.ParseQuery
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch


class MovieListPresenter(private val movieListView : MovieListContract.MovieListView, mDataBase : MovieDataBase) : MovieListContract.MovieListPresenter{

    private val movieDataBase = mDataBase
    private val movieRepository = MovieRepository
            .getInstance(MovieDataSource.getInstance(movieDataBase.movieDataDao()))

    override fun getLocalMovies() {
        val movieList : ArrayList<MovieModel> = ArrayList()
        launch(UI){
            val list = movieRepository.getAll()
            list.forEach {
                val movieModel = MovieModel()
                movieModel.year = it.year
                movieModel.poster = it.urlPost
                movieModel.title = it.title
                movieModel.movieId = it.movieId
                movieList.add(movieModel)
            }
            val fitlerLIst = movieList.distinctBy { it.movieId }
            movieListView.showMovies(fitlerLIst.toMutableList(), "Filmes carregados Localmente")
        }
    }

    override fun getParseMovies() {
        val query = ParseQuery<ParseObject>(ApiConstants.table)
        query.findInBackground { objects, e ->
            if (e == null) {
                val movieList : ArrayList<MovieModel> = ArrayList()
                objects!!.forEach{
                    val movieModel = MovieModel()
                    movieModel.movieId = it.getString(ApiConstants.movieId)!!
                    movieModel.title = it.getString(ApiConstants.title)!!
                    movieModel.poster = it.getString(ApiConstants.poster)!!
                    movieModel.year = it.getString(ApiConstants.year)!!
                    movieModel.isSaved = it.getBoolean(ApiConstants.saved)
                    movieList.add(movieModel)
                }
                val fitlerLIst = movieList.distinctBy { it.movieId }
                movieListView.showMovies(fitlerLIst.toMutableList(), "Filmes carregados Parse")
            } else {
                movieListView.errorParseMovies()
            }
        }
    }

}