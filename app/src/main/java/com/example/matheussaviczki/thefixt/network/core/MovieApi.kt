package com.example.matheussaviczki.thefixt.network.core

import com.example.matheussaviczki.thefixt.network.domain.MovieDetailResponse
import com.example.matheussaviczki.thefixt.network.domain.MovieResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieApi {

    companion object {
        const val TYPE_S = "s"
        const val TYPE_I = "i"
        const val SEARCH_ENDPOINT = "?type=movie"
        const val DETAIL_ENDPOINT = "?plot=full\""
    }

    @GET(SEARCH_ENDPOINT)
    fun getMovies(@Query(TYPE_S) title: String)
            : Single<MovieResponse>

    @GET(DETAIL_ENDPOINT)
    fun getMovieDetail(@Query(TYPE_I) ImdbId: String)
            : Single<MovieDetailResponse>

}