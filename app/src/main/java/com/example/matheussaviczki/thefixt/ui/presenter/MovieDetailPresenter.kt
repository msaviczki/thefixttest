package com.example.matheussaviczki.thefixt.ui.presenter

import com.example.matheussaviczki.thefixt.network.domain.ErrorResponse
import com.example.matheussaviczki.thefixt.network.domain.MovieDetailResponse
import com.example.matheussaviczki.thefixt.network.domain.MovieModel
import com.example.matheussaviczki.thefixt.support.constants.ApiConstants
import com.example.matheussaviczki.thefixt.support.repositories.entity.MovieData
import com.example.matheussaviczki.thefixt.support.repositories.entity.MovieDataSource
import com.example.matheussaviczki.thefixt.support.repositories.entity.MovieRepository
import com.example.matheussaviczki.thefixt.support.repositories.room.MovieDataBase
import com.example.matheussaviczki.thefixt.ui.base.UseCaseResult
import com.example.matheussaviczki.thefixt.ui.contract.MovieDetailContract
import com.example.matheussaviczki.thefixt.ui.usecase.MovieDetailUseCase
import com.parse.ParseObject
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch

class MovieDetailPresenter(private val detailView : MovieDetailContract.MovieDetailView, mDataBase : MovieDataBase) : MovieDetailContract.MovieDetailPresenter {

    private val detailUseCase : MovieDetailContract.MovieDetailUseCase = MovieDetailUseCase()
    private val movieDataBase = mDataBase
    private val movieRepository = MovieRepository
            .getInstance(MovieDataSource.getInstance(movieDataBase.movieDataDao()))

    override fun searchMovie(movieId: String) {
        detailUseCase.searchMovie(movieId, object : UseCaseResult<MovieDetailResponse>(){
            override fun onSuccess(apiResponse: MovieDetailResponse) {
                detailView.showMovieDetail(apiResponse)
            }

            override fun onError(errorModel: ErrorResponse) {
                detailView.onError()
            }

        })
    }

    override fun checkSaved(movieId: String) {
        launch(UI) {
            val movie = movieRepository.getById(movieId)
            if(movie != null){
                detailView.showSaveButton(true)
            } else{
                detailView.showSaveButton(false)
            }
        }
    }

    override fun saveMovie(movie: MovieModel) {
        val movieParse = ParseObject(ApiConstants.table)
        movieParse.put(ApiConstants.title, movie.title)
        movieParse.put(ApiConstants.year, movie.year)
        movieParse.put(ApiConstants.poster, movie.poster)
        movieParse.put(ApiConstants.movieId, movie.movieId)
        movieParse.put(ApiConstants.saved, movie.isSaved)
        movieParse.saveInBackground()
        val movieData = MovieData(0, movie.movieId, movie.title, movie.poster, movie.year, movie.isSaved)
        launch(UI) {
            movieRepository.insert(movieData)
        }
    }
}