package com.example.matheussaviczki.thefixt.app

import android.app.Application
import android.content.res.Configuration
import com.example.matheussaviczki.thefixt.support.constants.ApiConstants
import com.parse.Parse

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        Parse.initialize(Parse.Configuration.Builder(this)
                .applicationId(ApiConstants.apiKey)
                .clientKey(ApiConstants.clientKey)
                .server(ApiConstants.parseUrl).build()
        )
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
    }
}