package com.example.matheussaviczki.thefixt.support.customviews

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import com.example.matheussaviczki.thefixt.support.utils.C
import com.example.matheussaviczki.thefixt.support.utils.L
import com.example.matheussaviczki.thefixt.support.utils.closeKeyboard
import com.example.matheussaviczki.thefixt.support.utils.inflate
import kotlinx.android.synthetic.main.search.view.*

class CustomSearchView : LinearLayout {

    private lateinit var listener : SearchListener

    constructor(context: Context) : super(context){
        handleAttributes(context)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs){
        handleAttributes(context)
    }

    init {
        addView(inflate(L.search))
    }

    private fun handleAttributes(context: Context){
        val editText : EditText = searchView.findViewById(androidx.appcompat.R.id.search_src_text)
        val closeIcon : ImageView = searchView.findViewById(androidx.appcompat.R.id.search_close_btn)
        val searchIcon : ImageView = searchView.findViewById(androidx.appcompat.R.id.search_button)

        editText.setHintTextColor(ContextCompat.getColor(context, C.darkGrey))
        editText.setTextColor(Color.WHITE)
        closeIcon.setColorFilter(Color.WHITE)
        searchIcon.setColorFilter(Color.WHITE)

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(s: String): Boolean {
                return true
            }
            override fun onQueryTextSubmit(s: String): Boolean {
                closeKeyboard(context)
                listener.submitText(s)
                return true
            }
        })
    }

    fun setSearchListener(listener : SearchListener){
        this.listener = listener
    }

    interface SearchListener{
        fun submitText(text : String)
    }

}