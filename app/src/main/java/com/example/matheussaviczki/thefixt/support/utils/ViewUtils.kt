package com.example.matheussaviczki.thefixt.support.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.Animation
import android.view.inputmethod.InputMethodManager

fun View.show() {
    visibility = View.VISIBLE
}

fun View.hide() {
    visibility = View.GONE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun View.visible() = visibility == View.VISIBLE

fun View.onClick(function: () -> Unit) {
    setOnClickListener {
        function()
    }
}

fun View.scaleAnimation(pivotValueX : Float, pivotValueY: Float, duration : Long){
    clearAnimation()
    if(this.visibility != View.VISIBLE){
        val scaleAnim = android.view.animation.ScaleAnimation(
                0f, 1f,
                0f, 1f,
                Animation.RELATIVE_TO_SELF, pivotValueX,
                Animation.RELATIVE_TO_SELF, pivotValueY)
        scaleAnim.duration = duration
        scaleAnim.repeatCount = 0
        scaleAnim.interpolator = AccelerateDecelerateInterpolator()
        this.startAnimation(scaleAnim)
        this.show()
    }
}

infix fun ViewGroup.inflate(layoutResId: Int): View =
        LayoutInflater.from(context).inflate(layoutResId, this, false)

operator fun ViewGroup.get(index: Int): View = getChildAt(index)

fun View.closeKeyboard(context: Context) {
    val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
}