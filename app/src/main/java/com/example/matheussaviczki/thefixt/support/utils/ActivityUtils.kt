package com.example.matheussaviczki.thefixt.support.utils

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity

inline fun <reified T> AppCompatActivity.start() {
    this.startActivity(Intent(this, T::class.java))
}

inline fun <reified T> AppCompatActivity.startWithData(moveId : String) {
    val intent = Intent(this, T::class.java)
    intent.putExtra("movieId", moveId)
    this.startActivity(intent)
    overridePendingTransition(A.pos_left_right, A.neg_right_left)
}
