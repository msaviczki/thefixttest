package com.example.matheussaviczki.thefixt.ui.contract

import com.example.matheussaviczki.thefixt.network.domain.MovieModel
import com.example.matheussaviczki.thefixt.network.domain.MovieResponse
import com.example.matheussaviczki.thefixt.ui.base.*

interface HomeContract {

    interface HomeView : View {
        fun showMovies(movieModels : ArrayList<MovieModel>)
        fun onError()
    }

    interface HomePresenter : Presenter {
        fun searchMovies(title : String)
    }

    interface HomeUseCase : UseCase {
        fun searchMovies(title : String, resultHandler: UseCaseResult<MovieResponse>)
    }
}