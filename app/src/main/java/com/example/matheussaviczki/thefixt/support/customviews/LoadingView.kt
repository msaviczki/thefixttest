package com.example.matheussaviczki.thefixt.support.customviews

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import com.example.matheussaviczki.thefixt.support.utils.L
import com.example.matheussaviczki.thefixt.support.utils.inflate

class LoadingView : FrameLayout {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    init {
        addView(inflate(L.loading))
    }

}