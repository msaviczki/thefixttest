package com.example.matheussaviczki.thefixt.support.utils

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SchedulersManager {
    fun getProcessScheduler(): Scheduler = Schedulers.io()
    fun getAndroidScheduler(): Scheduler = AndroidSchedulers.mainThread()
}