package com.example.matheussaviczki.thefixt.support.repositories.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.matheussaviczki.thefixt.support.repositories.dao.MovieDataDao
import com.example.matheussaviczki.thefixt.support.repositories.entity.MovieData

@Database(entities = [MovieData::class], version = 1, exportSchema = false)
abstract class MovieDataBase : RoomDatabase() {

    abstract fun movieDataDao() : MovieDataDao

    companion object {
        val DATABASE_NAME = "Movie_Database-ROOM"
        private var INSTANCE : MovieDataBase? = null

        fun getInstance(context : Context) : MovieDataBase{
            if(INSTANCE == null)
                INSTANCE =  Room.databaseBuilder(context, MovieDataBase::class.java, DATABASE_NAME)
                        .fallbackToDestructiveMigration()
                        .build()

                return INSTANCE!!
        }
    }
}