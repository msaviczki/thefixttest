package com.example.matheussaviczki.thefixt.support.utils

import android.content.Context
import android.widget.Toast


fun Int.toPx(context: Context): Int {
    val density = context.resources.displayMetrics.density
    return (this * density).toInt()
}

fun Int.toDp(context: Context): Int {
    val density = context.resources.displayMetrics.density
    return (this / density).toInt()
}

fun toast(context: Context, message: String, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(context, message, duration).show()
}

fun Any.isNull() : Boolean{
    if(this == null)
        return true
    return false
}