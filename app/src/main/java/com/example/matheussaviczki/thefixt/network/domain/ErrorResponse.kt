package com.example.matheussaviczki.thefixt.network.domain

import com.example.matheussaviczki.thefixt.ui.base.BaseResponse

class ErrorResponse : BaseResponse {
    val error = "Erro ao fazer requisição"
}