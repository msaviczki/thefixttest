package com.example.matheussaviczki.thefixt.support.repositories.entity

import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch


class MovieRepository(private val movieDataSourceContract : MovieDataSourceContract) : MovieDataSourceContract {

    override suspend fun getById(movieId : String): MovieData {
        return async {
            movieDataSourceContract.getById(movieId)
        }.await()
    }

    override suspend fun getAll(): List<MovieData> {
        return async {
            movieDataSourceContract.getAll()
        }.await()
    }

    override fun insert(movieData: MovieData) : Job {
        return launch {
            movieDataSourceContract.insert(movieData)
        }
    }

    override fun deleteAll() : Job {
        return launch {
            movieDataSourceContract.deleteAll()
        }
    }

    companion object {
        var INSTANCE : MovieRepository? = null

        fun getInstance(movieDataSourceContract : MovieDataSourceContract) : MovieRepository{
            if(INSTANCE == null)
                INSTANCE = MovieRepository(movieDataSourceContract)

            return INSTANCE!!
        }
    }
}