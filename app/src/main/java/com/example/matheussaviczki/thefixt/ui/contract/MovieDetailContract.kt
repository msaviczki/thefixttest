package com.example.matheussaviczki.thefixt.ui.contract

import com.example.matheussaviczki.thefixt.network.domain.MovieDetailResponse
import com.example.matheussaviczki.thefixt.network.domain.MovieModel
import com.example.matheussaviczki.thefixt.ui.base.Presenter
import com.example.matheussaviczki.thefixt.ui.base.UseCase
import com.example.matheussaviczki.thefixt.ui.base.UseCaseResult
import com.example.matheussaviczki.thefixt.ui.base.View

interface MovieDetailContract {

    interface MovieDetailView : View {
        fun showSaveButton(save : Boolean)
        fun showMovieDetail(detail : MovieDetailResponse)
        fun onError()
    }

    interface MovieDetailPresenter : Presenter {
        fun searchMovie(movieId : String)
        fun saveMovie(movie : MovieModel)
        fun checkSaved(movieId : String)
    }

    interface MovieDetailUseCase : UseCase {
        fun searchMovie(movieId : String, resultHandler: UseCaseResult<MovieDetailResponse>)
    }
}