package com.example.matheussaviczki.thefixt.network.domain

import com.example.matheussaviczki.thefixt.ui.base.BaseResponse
import com.example.matheussaviczki.thefixt.support.adapter.BaseListAdapter
import com.google.gson.annotations.SerializedName

class MovieModel(override val type: Int = 0) :  BaseListAdapter.ItemView, BaseResponse {

    @SerializedName("Title")
    var title : String = ""

    @SerializedName("Year")
    var year : String = ""

    @SerializedName("Poster")
    var poster : String = ""

    @SerializedName("imdbID")
    var movieId : String = ""

    var isSaved : Boolean = false
}