package com.example.matheussaviczki.thefixt.support.repositories.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.example.matheussaviczki.thefixt.support.repositories.entity.MovieData

@Dao
interface MovieDataDao {

    @Query("SELECT * from movie_table")
    fun getAll(): List<MovieData>

    @Query("SELECT * from movie_table where movieId=:movieId")
    fun getById(movieId : String) : MovieData

    @Insert(onConflict = REPLACE)
    fun insert(movieData: MovieData)

    @Query("DELETE from movie_table")
    fun deleteAll()

}