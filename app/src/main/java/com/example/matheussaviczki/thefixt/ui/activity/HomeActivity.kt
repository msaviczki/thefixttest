package com.example.matheussaviczki.thefixt.ui.activity

import com.example.matheussaviczki.thefixt.network.domain.MovieModel
import com.example.matheussaviczki.thefixt.ui.adapter.MovieListAdapter
import com.example.matheussaviczki.thefixt.ui.base.BaseActivity
import com.example.matheussaviczki.thefixt.ui.contract.HomeContract
import com.example.matheussaviczki.thefixt.ui.presenter.HomePresenter
import kotlinx.android.synthetic.main.activity_home.*
import androidx.recyclerview.widget.GridLayoutManager
import com.example.matheussaviczki.thefixt.support.customviews.CustomSearchView
import com.example.matheussaviczki.thefixt.support.utils.*


class HomeActivity : BaseActivity(), HomeContract.HomeView, CustomSearchView.SearchListener, MovieListAdapter.SaveMovieListener {

    override val layout: Int = L.activity_home

    lateinit var adapter : MovieListAdapter

    private val homePresenter : HomeContract.HomePresenter by lazy {
        HomePresenter(this)
    }

    override fun init() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        search.setSearchListener(this)
        adapter = MovieListAdapter(this)
        adapter.clearItems()
        recyclerMovieList.adapter = adapter
        recyclerMovieList.layoutManager = GridLayoutManager(this, 2)
        fabMovieList.onClick { start<MovieListActivity>() }
    }

    override fun showMovieDetailOnClick(movie: MovieModel) {
        startWithData<MovieDetailActivity>(movie.movieId)
    }

    override fun submitText(text: String) {
        txtMovieError.hide()
        recyclerMovieList.hide()
        adapter.clearItems()
        loading.show()
        homePresenter.searchMovies(text)
    }

    override fun showMovies(movieModels: ArrayList<MovieModel>) {
        if(movieModels.isNotEmpty()){
            adapter.addItems(movieModels)
            adapter.notifyDataSetChanged()
            loading.hide()
            recyclerMovieList.show()
        } else {
            loading.hide()
            txtMovieError.text = getString(S.movie_not_found)
            txtMovieError.show()
        }
    }

    override fun onError() {
        loading.hide()
        txtMovieError.text = getString(S.error)
        txtMovieError.show()
    }

}
