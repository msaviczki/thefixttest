package com.example.matheussaviczki.thefixt.network.core

import retrofit2.Retrofit

class ClientApi {

    companion object {
        val client : Retrofit by lazy {
            ApiBuilder(ApiConstants.BASE_URL).client
        }

        val movieApi : MovieApi by lazy {
            client.create(MovieApi::class.java)
        }
    }
}