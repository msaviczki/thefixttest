package com.example.matheussaviczki.thefixt.support.repositories.entity

import com.example.matheussaviczki.thefixt.support.repositories.dao.MovieDataDao
import kotlinx.coroutines.experimental.DefaultDispatcher
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.withContext

class MovieDataSource(private val movieDAO : MovieDataDao) : MovieDataSourceContract {

    override suspend fun getAll(): List<MovieData> {
        return withContext(DefaultDispatcher) {
            movieDAO.getAll()
        }
    }

    override suspend fun getById(movieId: String): MovieData {
        return withContext(DefaultDispatcher) {
            movieDAO.getById(movieId)
        }
    }

    override fun insert(movieData: MovieData): Job {
        return launch {
            movieDAO.insert(movieData)
        }
    }

    override fun deleteAll(): Job {
        return launch {
            movieDAO.deleteAll()
        }
    }

    companion object {
        private var INSTANCE : MovieDataSource? = null

        fun getInstance(movieDAO: MovieDataDao) : MovieDataSource{
            if(INSTANCE == null)
                INSTANCE = MovieDataSource(movieDAO)

            return INSTANCE!!
        }
    }
}