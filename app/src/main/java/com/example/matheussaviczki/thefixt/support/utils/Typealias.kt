package com.example.matheussaviczki.thefixt.support.utils

import com.example.matheussaviczki.thefixt.R

/**
 * Created by Matheus Saviczki on 28.06.2018.
 */

typealias S = R.string
typealias C = R.color
typealias D = R.drawable
typealias L = R.layout
typealias A = R.anim