package com.example.matheussaviczki.thefixt.ui.adapter

import android.view.ViewGroup
import com.example.matheussaviczki.thefixt.network.domain.MovieModel
import com.example.matheussaviczki.thefixt.support.utils.L
import com.example.matheussaviczki.thefixt.support.adapter.BaseListAdapter
import com.example.matheussaviczki.thefixt.support.utils.inflate

class MovieListAdapter(private val saveMovieListener : SaveMovieListener) : BaseListAdapter() {

    override fun getItemViewHolder(parent: ViewGroup): BaseViewHolder<ItemView> {
        return MovieListViewHolder(parent.inflate(L.card_detail), saveMovieListener) as BaseViewHolder<ItemView>
    }

    interface SaveMovieListener{
        fun showMovieDetailOnClick(movie : MovieModel)
    }
}