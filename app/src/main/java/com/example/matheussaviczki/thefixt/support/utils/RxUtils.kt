package com.example.matheussaviczki.thefixt.support.utils

import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable

fun <T> Single<T>.rxObserve(onSuccess: (T) -> Unit, onFailure: (Throwable) -> Unit,
                            processScheduler: Scheduler,
                            androidScheduler: Scheduler = AndroidSchedulers.mainThread()
): Disposable =
    subscribeOn(processScheduler)
            .observeOn(androidScheduler)
            .subscribe(onSuccess, onFailure)