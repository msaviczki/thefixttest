package com.example.matheussaviczki.thefixt.ui.base

import com.example.matheussaviczki.thefixt.network.domain.ErrorResponse

abstract class UseCaseResult<T : BaseResponse> {
    abstract fun onSuccess(apiResponse: T)
    abstract fun onError(errorModel: ErrorResponse)
}