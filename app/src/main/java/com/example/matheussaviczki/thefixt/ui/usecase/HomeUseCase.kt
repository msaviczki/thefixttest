package com.example.matheussaviczki.thefixt.ui.usecase

import com.example.matheussaviczki.thefixt.network.core.ClientApi
import com.example.matheussaviczki.thefixt.network.core.MovieApi
import com.example.matheussaviczki.thefixt.network.domain.MovieResponse
import com.example.matheussaviczki.thefixt.ui.base.BaseUseCase
import com.example.matheussaviczki.thefixt.ui.base.UseCaseResult
import com.example.matheussaviczki.thefixt.ui.contract.HomeContract

class HomeUseCase : BaseUseCase(), HomeContract.HomeUseCase {

    private val movieApi : MovieApi = ClientApi.movieApi

    override fun searchMovies(title: String, resultHandler: UseCaseResult<MovieResponse>) {
        movieApi.getMovies(title)
                .handleResult(resultHandler)
    }
}