package com.example.matheussaviczki.thefixt.ui.base

interface UseCase {
    fun cleanUp()
}
