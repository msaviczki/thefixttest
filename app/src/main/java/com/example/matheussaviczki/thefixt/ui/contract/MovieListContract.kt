package com.example.matheussaviczki.thefixt.ui.contract

import com.example.matheussaviczki.thefixt.network.domain.MovieModel
import com.example.matheussaviczki.thefixt.ui.base.Presenter
import com.example.matheussaviczki.thefixt.ui.base.View

interface MovieListContract {

    interface MovieListView : View {
        fun showMovies(movieModels : MutableList<MovieModel>, loadedFrom : String)
        fun errorParseMovies()
    }

    interface MovieListPresenter : Presenter {
        fun getParseMovies()
        fun getLocalMovies()
    }
}