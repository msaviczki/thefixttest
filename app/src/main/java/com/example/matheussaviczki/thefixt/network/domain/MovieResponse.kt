package com.example.matheussaviczki.thefixt.network.domain

import com.example.matheussaviczki.thefixt.ui.base.BaseResponse
import com.google.gson.annotations.SerializedName

class MovieResponse : BaseResponse {
    @SerializedName("Search")
    var movieModelList : ArrayList<MovieModel> = ArrayList()
}