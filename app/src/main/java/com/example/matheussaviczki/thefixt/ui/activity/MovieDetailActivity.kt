package com.example.matheussaviczki.thefixt.ui.activity

import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.matheussaviczki.thefixt.network.domain.MovieDetailResponse
import com.example.matheussaviczki.thefixt.network.domain.MovieModel
import com.example.matheussaviczki.thefixt.support.repositories.room.MovieDataBase
import com.example.matheussaviczki.thefixt.support.utils.*
import com.example.matheussaviczki.thefixt.ui.base.BaseActivity
import com.example.matheussaviczki.thefixt.ui.contract.MovieDetailContract
import com.example.matheussaviczki.thefixt.ui.presenter.MovieDetailPresenter
import kotlinx.android.synthetic.main.activity_moviedetail.*

class MovieDetailActivity : BaseActivity(), MovieDetailContract.MovieDetailView {

    override val layout: Int = L.activity_moviedetail

    private val detailPresenter : MovieDetailContract.MovieDetailPresenter by lazy {
        MovieDetailPresenter(this, MovieDataBase.getInstance(this))
    }

    private var movieId = ""
    private var isSaved = false

    override fun init() {
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        supportActionBar?.setDisplayShowTitleEnabled(false)
        movieId = intent.getStringExtra("movieId")
        detailPresenter.checkSaved(movieId)
    }

    override fun showSaveButton(save: Boolean) {
        isSaved = save
        if(isSaved){ imgStar.show() }
        detailPresenter.searchMovie(movieId)
    }

    override fun showMovieDetail(detail: MovieDetailResponse) {
        val movie = MovieModel()
        detail.apply {
            val request  = RequestOptions()
            request.centerCrop()
            Glide.with(this@MovieDetailActivity)
                    .asBitmap()
                    .load(poster)
                    .apply(request)
                    .into(imgPoster)
            toolbarTitle.text = title
            txtTitle.text = title
            txtGenre.text = genre
            txtImdbRate.text = imdbRating
            txtDuration.text = runtime
            txtRealese.text = released
            txtDescription.text = plot
            txtDirector.text = director
            txtWriter.text = writer
            txtActors.text = actors
            movie.isSaved = true
            movie.poster = poster
            movie.movieId = movieId
            movie.year = year
            movie.title = title
        }

        frameSave.onClick {
            if(!isSaved){
                saveMovie(movie)
            }
        }
    }

    private fun saveMovie(movie : MovieModel){
        imgStar.scaleAnimation(PIVOT_X, PIVOT_Y,
                DURATION.toLong())
        frameSave.isClickable = false
        detailPresenter.saveMovie(movie)
    }

    override fun onError() {
        scroll.hide()
        frameSave.hide()
        txtDetailMovieError.show()
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(A.neg_left_right, A.pos_right_left)
    }

    companion object {
        const val PIVOT_X = 0.5F
        const val PIVOT_Y = 0.5F
        const val DURATION = 500
    }

}