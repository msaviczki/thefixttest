package com.example.matheussaviczki.thefixt.support.repositories.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "movie_table")
class MovieData(@PrimaryKey(autoGenerate = true)
                      val id: Int,
                val movieId : String,
                val title: String,
                val urlPost: String,
                val year: String,
                val isSaved : Boolean)
