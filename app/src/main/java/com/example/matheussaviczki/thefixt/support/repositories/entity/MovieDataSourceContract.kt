package com.example.matheussaviczki.thefixt.support.repositories.entity

import kotlinx.coroutines.experimental.Job

interface MovieDataSourceContract{
    suspend fun getAll(): List<MovieData>
    suspend fun getById(movieId : String) : MovieData
    fun insert(movieData: MovieData) : Job
    fun deleteAll(): Job
}