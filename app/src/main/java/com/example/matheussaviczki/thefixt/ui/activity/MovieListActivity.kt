package com.example.matheussaviczki.thefixt.ui.activity

import androidx.recyclerview.widget.GridLayoutManager
import com.example.matheussaviczki.thefixt.network.domain.MovieModel
import com.example.matheussaviczki.thefixt.support.repositories.room.MovieDataBase
import com.example.matheussaviczki.thefixt.support.utils.*
import com.example.matheussaviczki.thefixt.ui.adapter.MovieListAdapter
import com.example.matheussaviczki.thefixt.ui.base.BaseActivity
import com.example.matheussaviczki.thefixt.ui.contract.MovieListContract
import com.example.matheussaviczki.thefixt.ui.presenter.MovieListPresenter
import kotlinx.android.synthetic.main.activity_movielist.*

class MovieListActivity : BaseActivity(), MovieListContract.MovieListView, MovieListAdapter.SaveMovieListener {

    override val layout: Int = L.activity_movielist

    lateinit var adapter : MovieListAdapter

    private val moviePresenter : MovieListContract.MovieListPresenter by lazy {
        MovieListPresenter(this,  MovieDataBase.getInstance(this))
    }

    override fun init() {
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        loading.show()
        supportActionBar?.setDisplayShowTitleEnabled(false)
        adapter = MovieListAdapter(this)
        adapter.clearItems()
        recyclerMovieList.adapter = adapter
        recyclerMovieList.layoutManager = GridLayoutManager(this, 2)
        moviePresenter.getParseMovies()
    }

    override fun showMovies(movieModels: MutableList<MovieModel>, loadedFrom : String) {
        txtLoadedFrom.text = loadedFrom
        if(movieModels.isNotEmpty()){
            adapter.addItems(movieModels)
            adapter.notifyDataSetChanged()
            recyclerMovieList.show()
        } else {
            txtMovieListError.text = getString(S.not_saved_movie)
            txtMovieListError.show()
        }
        loading.hide()
    }

    override fun errorParseMovies() {
        moviePresenter.getLocalMovies()
    }

    override fun showMovieDetailOnClick(movie: MovieModel) {
        startWithData<MovieDetailActivity>(movie.movieId)
    }
}